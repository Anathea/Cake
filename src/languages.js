export default {
    en: { code: 'GB', name: 'English' },
    fr: { code: 'FR', name: 'Français'},
    pl: { code: 'PL', name: 'Polski' },
};
