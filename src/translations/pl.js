module.exports = {
    'title': 'Ciasto Warstwowe Pociągu',
    'description': 'Nasza seksualność jest bardziej złożona niż tylko podział hetero/bi/homo. Tutaj możesz opisać ją na trech osiach: typu pociągu, typu związki, oraz orientacji.',
    'keywords': 'pociąg, związek, orientacja, seksaulność, orientacja seksualna, skala, skala kinsey\'a, aseksualność, aseksualny, monogamia, polyamoria',

    'footer.author': 'Stworzone z ❤ przez',
    'footer.base': 'na podstawie',
    'footer.diagram': 'diagramu stworzonego przez Lunę Rudd',
    'footer.donate': 'Postaw mi piwo',
    'footer.source': 'Kod źródłowy',

    'attraction.label': 'Typ pociągu',
    'attraction.b.name': 'Aseksualność aromantyczna',
    'attraction.b.description': 'Brak prociągu seksualnego i romantycznego, pociąg jedynie platoniczy lub estetyczny.',
    'attraction.g.name': 'Aseksualność romantyczna',
    'attraction.g.description': 'Otwarta/y na romans, pewne rodzaje dotyku i głębokie więzi emocjonalne, ' +
                                'ale nie zainteresowana/y związkami seksualnymi.',
    'attraction.o.name': 'Seksualność trzeciorzędna',
    'attraction.o.description': 'Nie odczuwa pociągu seksualnego, ale otwarta/y na stosunki seksaulne ' +
                                'dla konkretnych celów (prokreacja, uszczęśliwianie partnerów, itp.)',
    'attraction.p.name': 'Seksualność drugorzędna',
    'attraction.p.description': 'Więź romantyczna, platoniczna i estetyczna są łatwe do nawiązania, ' +
                                'natomiast pociąg seksualny rozwija się w miarę rozwoju związku, lecz nie od razu.',
    'attraction.r.name': 'Seksualność pierwszorzędna',
    'attraction.r.description': 'Pociąg seksualny istnieje od razu, choć nie muszą z niego wynikać działania. ' +
                                'Inne składniki związku, jak więź i przyjaźń, również są niezbędne.',
    'attraction.c.name': 'Seksualność aromantyczna',
    'attraction.c.description': 'Głównym celem związków jest seksualność. ' +
                                'Romans nie jest szczególnie pożądany.',

    'relationship.label': 'Typ związku',
    'relationship.A.name': 'Ściśle monogamiczna/y',
    'relationship.A.description': 'Wchodzi w związki z jedną osobą na raz ' +
                                    'w celu zbudowania jednego związku na całe życie.',
    'relationship.B.name': 'Głównie monogamiczna/y',
    'relationship.B.description': 'Pragnie przede wszystkim związków monogamicznych, ' +
                                    'ale jest otwarta/y na poliamorię dla uszczęśliwienia partnerki/a ' +
                                    'lub z innych powodów.',
    'relationship.C.name': 'Bez preferencji',
    'relationship.C.description': 'Otwarta/y zarówno na poliamorię, jak i monogamię, indywidualnie dla każdego przypadku ' +
                                    'w zależności od pragnień zaangażowanych partnerów.',
    'relationship.D.name': 'Głównie poliamoryczna/y',
    'relationship.D.description': 'Pragnie przede wszystkim związków poliamorycznych, ' +
                                    'ale jest otwarta/y na monogamię dla uszczęśliwienia partnerki/a ' +
                                    'lub z innych powodów.',
    'relationship.E.name': 'Ściśle poliamoryczna/y',
    'relationship.E.description': 'Pożąda wielu jednoczesnych związków z różnymi partnerami ' +
                                    'w celu zaspokojenia różnych potrzeb i swobodnego okazywania pociągu wielu osobom.',

    'orientation.label': 'Typ orientacji',
    'orientation.0.name': 'Wyłącznie heteroseksualna/y',
    'orientation.0.description': 'Zainteresowana/y wyłącznie osobami odmiennej/ych płci.',
    'orientation.1.name': 'Głównie heteroseksualna/y',
    'orientation.1.description': 'Zainteresowana/y głównie osobami odmiennej/ych płci.',
    'orientation.2.name': 'Lekko heteroseksuualna/y',
    'orientation.2.description': 'Woli partnerki/ów odmiennej/ych płci, ' +
                                    'ale również zainteresowana/y osobami tej samej płci.',
    'orientation.3.name': 'Biseksualna/y / Panseksualny/a',
    'orientation.3.description': 'Pociąg nie jest uzależniony od płci.',
    'orientation.4.name': 'Lekko homoseksualna/y',
    'orientation.4.description': 'Woli partnerki/ów tej samej płci, ' +
                                    'ale również zainteresowana/y osobami odmiennej/ych płci.',
    'orientation.5.name': 'Głównie homoseksualna/y',
    'orientation.5.description': 'Zainteresowana/y głównie osobami tej samej płci.',
    'orientation.6.name': 'Wyłącznie homoseksualna/y',
    'orientation.6.description': 'Zainteresowana/y wyłącznie osobami tej samej płci.',

    'show.own': 'Też wybierz swój kawałek',

    'url.empty': 'Wybierz opcje w każdej z trzech kolumn, aby wygenerować link.',
    'url.copy': 'Skopiuj do schowka',

    'share.text': 'Mój rodzaj pociągu to {code}, a Twój?',
    'share.text.rich': 'Mój rodzaj pociągu to <strong>{code}</strong>.<br/>A Twój?',
    'share.facebook': 'Udostępnij na Facebooku',
    'share.twitter': 'Udostępnij na Twitterze',

    'privacy.header': 'Prywatność',
    'privacy.noData': 'Wszystkie dane o opcjach, które zaznaczyłaś/eś, są zakodowane w URL-u i nie są nigdzie zapisywane.',
    'privacy.tracking': 'Używam Matomo do analizy ruchu na stronie. Wszystkie dane odwiedzających są anonimizowane.',
    'privacy.contact': 'Jeśli maszs pytania lub wnioski na temat Twoich danych osobowych, możesz skontaktować się ze mną przez ',
    'privacy.home': 'Wróć na stronę główną',
};

