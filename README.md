# Attraction Layer Cake

 - [Website](https://cake.avris.it)

## Local copy

    make install
    make run
    
## Copyright
 
 * **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
 * **Based on:** [a diagram by Luna Rudd](https://imgur.com/gallery/YAGLE)
 * **Licence:** [MIT](https://mit.avris.it)
